package transport.army;

public class AmphibianTank extends Tank{

	private String  destinationPort="Antwerp";
	public void sail(){
		System.out.println("Sail away...");
	}

	@Override
	public String toString() {
		return "AmphibianTank{" +
			"destinationPort='" + destinationPort + '\'' +
			"} " + super.toString();
	}


}
